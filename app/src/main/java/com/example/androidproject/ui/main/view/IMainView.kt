package com.example.androidproject.ui.main.view

import com.example.androidproject.ui.base.view.IView

interface IMainView : IView {

    fun openTrend(pageNumber: Int)
    fun openEvents(pageNumber: Int)
    fun openAdd(pageNumber: Int)
    fun openNotification(pageNumber: Int)
    fun openSettings(pageNumber: Int)

}