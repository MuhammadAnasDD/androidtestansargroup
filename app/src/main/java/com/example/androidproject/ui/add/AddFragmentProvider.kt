package com.example.androidproject.ui.add

import com.example.androidproject.ui.add.view.AddFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class AddFragmentProvider {

    @ContributesAndroidInjector(modules = [AddFragmentModule::class])
    internal abstract fun provideAddFragmentFactory(): AddFragment
}