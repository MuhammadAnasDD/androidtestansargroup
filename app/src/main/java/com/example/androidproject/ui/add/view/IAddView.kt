package com.example.androidproject.ui.add.view


import com.example.androidproject.data.network.model.CategoryResponse
import com.example.androidproject.data.network.model.ResultData
import com.example.androidproject.ui.base.view.IView

interface IAddView : IView {
    fun showCategoryList(categorise: List<ResultData>?)
}