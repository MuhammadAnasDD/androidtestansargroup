package com.example.androidproject.ui.events.presenter

import com.example.androidproject.ui.base.presenter.IPresenter
import com.example.androidproject.ui.events.interactor.IEventsInteractor
import com.example.androidproject.ui.events.view.IEventsView

interface IEventsPresenter<V : IEventsView, I : IEventsInteractor> : IPresenter<V, I> {

}