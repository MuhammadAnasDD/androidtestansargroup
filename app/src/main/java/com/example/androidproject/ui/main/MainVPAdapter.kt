package com.example.androidproject.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import android.view.ViewGroup
import com.example.androidproject.ui.add.view.AddFragment
import com.example.androidproject.ui.events.view.EventsFragment

class MainVPAdapter(fragmentManager: FragmentManager, val context: Context) : FragmentStatePagerAdapter(fragmentManager) {
    var mCurrentFragment: Fragment? = null

    companion object {
        const val TREND_FRAGMENT_INDEX = 0
        const val EVENT_FRAGMENT_INDEX = 1
        const val ADD_FRAGMENT_INDEX = 2
        const val NOTIFICATION_FRAGMENT_INDEX = 3
        const val SETTINGS_FRAGMENT_INDEX = 4
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            TREND_FRAGMENT_INDEX -> EventsFragment.newInstance()
            EVENT_FRAGMENT_INDEX -> EventsFragment.newInstance()
            ADD_FRAGMENT_INDEX -> AddFragment.newInstance()
            NOTIFICATION_FRAGMENT_INDEX -> EventsFragment.newInstance()
            SETTINGS_FRAGMENT_INDEX ->EventsFragment.newInstance()
            else -> Fragment()
        }
    }

    override fun getCount(): Int = 5

    override fun setPrimaryItem(container: ViewGroup, position: Int, object1: Any) {
        if (mCurrentFragment != object1) {
            mCurrentFragment = object1 as Fragment
        }
        super.setPrimaryItem(container, position, object1)
    }

}