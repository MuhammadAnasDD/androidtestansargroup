package com.example.androidproject.ui.generalInformation.presenter

import com.example.androidproject.ui.base.presenter.BasePresenterImpl
import com.example.androidproject.ui.generalInformation.interactor.IGeneralInformationInteractor
import com.example.androidproject.ui.generalInformation.view.IGeneralInformationView
import com.example.androidproject.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class GeneralInformationPresenterImpl<V : IGeneralInformationView, I : IGeneralInformationInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenterImpl<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), IGeneralInformationPresenter<V, I> {

    override fun setView() {
        getView()?.setUpView()
    }
}
