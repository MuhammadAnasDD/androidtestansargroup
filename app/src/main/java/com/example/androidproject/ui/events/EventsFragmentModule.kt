package com.example.androidproject.ui.events

import com.example.androidproject.ui.events.interactor.EventsInteractorImpl
import com.example.androidproject.ui.events.interactor.IEventsInteractor
import com.example.androidproject.ui.events.presenter.EventsPresenterImpl
import com.example.androidproject.ui.events.presenter.IEventsPresenter
import com.example.androidproject.ui.events.view.IEventsView
import dagger.Module
import dagger.Provides

@Module
class EventsFragmentModule {

    @Provides
    internal fun provideEventsInteractor(eventsInteractorImpl: EventsInteractorImpl): IEventsInteractor = eventsInteractorImpl

    @Provides
    internal fun provideEventsPresenter(eventsPresenterImpl: EventsPresenterImpl<IEventsView, IEventsInteractor>)
            : IEventsPresenter<IEventsView, IEventsInteractor> = eventsPresenterImpl
}