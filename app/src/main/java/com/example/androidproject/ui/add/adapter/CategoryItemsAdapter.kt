package com.example.androidproject.ui.add.adapter


import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.androidproject.R
import com.example.androidproject.data.network.model.ResultData
import com.example.androidproject.util.extension.gone
import com.example.androidproject.util.extension.visible
import kotlinx.android.synthetic.main.item_row.view.*

class CategoryItemsAdapter(val mItems: MutableList<ResultData>) : RecyclerView.Adapter<CategoryItemsAdapter.ItemVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVH = ItemVH(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row, parent, false))

    override fun getItemCount(): Int = mItems.size

    override fun onBindViewHolder(holder: ItemVH, position: Int) = mItems[position]?.let{
        holder.onBind(it)
    }

    inner class ItemVH(view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(data: ResultData) {

            data.title?.let {
                itemView.category.text = it
            }

            if (data.isOpenSubCategory){
                itemView.subCategoryRecyclerView.visible()
            }else itemView.subCategoryRecyclerView.gone()

            itemView.setOnClickListener{
                if (data.isOpenSubCategory){
                    data.isOpenSubCategory = false
                    itemView.subCategoryRecyclerView.gone()
                }else {
                    data.isOpenSubCategory = true
                    itemView.subCategoryRecyclerView.visible()
                }
                notifyItemChanged(adapterPosition)
            }


            val levelsItemsAdapter = data.subCategories?.let {
                SubCategoryItemsAdapter(it.toMutableList())
            }
            itemView.subCategoryRecyclerView.adapter = levelsItemsAdapter
        }

    }
}