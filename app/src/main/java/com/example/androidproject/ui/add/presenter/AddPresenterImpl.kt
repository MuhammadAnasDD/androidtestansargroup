package com.example.androidproject.ui.add.presenter

import com.example.androidproject.ui.base.presenter.BasePresenterImpl
import com.example.androidproject.ui.add.interactor.IAddInteractor
import com.example.androidproject.ui.add.view.IAddView
import com.example.androidproject.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddPresenterImpl<V : IAddView, I : IAddInteractor>
@Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenterImpl<V, I>(
        interactor = interactor,
        schedulerProvider = schedulerProvider,
        compositeDisposable = disposable
    ),
    IAddPresenter<V, I> {

    override fun getCategories() {
        interactor?.let {
            compositeDisposable.add(
                it.getCategoryResponse()
                    .compose(schedulerProvider.ioToMainSingleScheduler())
                    .subscribe({ result ->
                        getView()?.showCategoryList(result.result)
                    }, { throwable ->
                        // handle error
                        handleThrowable(throwable)
                    })
            )
        }
    }
}