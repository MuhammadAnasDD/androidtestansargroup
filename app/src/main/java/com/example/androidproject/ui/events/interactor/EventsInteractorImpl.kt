package com.example.androidproject.ui.events.interactor

import com.example.androidproject.data.network.ApiHelper
import com.example.androidproject.data.preferences.PreferenceHelper
import com.example.androidproject.ui.base.interactor.BaseInteractorImpl
import javax.inject.Inject

class EventsInteractorImpl @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractorImpl(preferenceHelper = preferenceHelper, apiHelper = apiHelper), IEventsInteractor {

}