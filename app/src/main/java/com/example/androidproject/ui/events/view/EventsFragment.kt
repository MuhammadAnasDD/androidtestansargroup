package com.example.androidproject.ui.events.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.androidproject.R
import com.example.androidproject.ui.base.view.BaseFragment
import com.example.androidproject.ui.events.interactor.IEventsInteractor
import com.example.androidproject.ui.events.presenter.IEventsPresenter
import javax.inject.Inject

class EventsFragment : BaseFragment(), IEventsView {

    @Inject
    internal lateinit var presenter: IEventsPresenter<IEventsView, IEventsInteractor>

    companion object {
        fun newInstance(): EventsFragment {
            val args = Bundle()
            val fragment = EventsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_trend, container, false)
        presenter.onAttach(this)
        return view
    }

    override fun setUp() {

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
    }


    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

}
