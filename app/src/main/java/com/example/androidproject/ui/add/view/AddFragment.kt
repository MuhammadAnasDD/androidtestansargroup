package com.example.androidproject.ui.add.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.androidproject.R
import com.example.androidproject.data.network.model.ResultData
import com.example.androidproject.ui.add.adapter.CategoryItemsAdapter
import com.example.androidproject.ui.base.view.BaseFragment
import com.example.androidproject.ui.add.interactor.IAddInteractor
import com.example.androidproject.ui.add.presenter.IAddPresenter
import kotlinx.android.synthetic.main.fragment_trend.*
import javax.inject.Inject


class AddFragment : BaseFragment(), IAddView {

    @Inject
    internal lateinit var presenter: IAddPresenter<IAddView, IAddInteractor>

    companion object {
        const val REQUEST_ADD_CAR = 150
        var openOrderLPGScreen: Boolean? = null
        var openOrderOnSpot: Boolean? = null
        fun newInstance(): AddFragment {
            val args = Bundle()
            val fragment = AddFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_trend, container, false)
        presenter.onAttach(this)
        return view
    }

    override fun setUp() {
        presenter.getCategories()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
    }


    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun showCategoryList(categorise: List<ResultData>?) {
        categorise?.let {
            val servicesAdapter = CategoryItemsAdapter(it.toMutableList())
            categoryRecycler.layoutManager = LinearLayoutManager(activity)
            categoryRecycler.adapter = servicesAdapter
        }
    }

}
