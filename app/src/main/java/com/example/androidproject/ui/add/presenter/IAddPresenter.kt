package com.example.androidproject.ui.add.presenter

import com.example.androidproject.ui.base.presenter.IPresenter
import com.example.androidproject.ui.add.interactor.IAddInteractor
import com.example.androidproject.ui.add.view.IAddView


interface IAddPresenter<V : IAddView, I : IAddInteractor> : IPresenter<V, I> {
        fun getCategories()
}