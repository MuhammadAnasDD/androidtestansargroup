package com.example.androidproject.ui.main.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.androidproject.R
import com.example.androidproject.ui.base.view.BaseActivity
import com.example.androidproject.ui.main.MainVPAdapter
import com.example.androidproject.ui.main.interactor.IMainInteractor
import com.example.androidproject.ui.main.presenter.IMainPresenter
import com.example.androidproject.util.extension.removeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.google.gson.Gson
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : BaseActivity(), IMainView, HasSupportFragmentInjector, View.OnClickListener {

    @Inject
    internal lateinit var presenter: IMainPresenter<IMainView, IMainInteractor>

    lateinit var mainVPAdapter: MainVPAdapter

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mGson: Gson

    companion object {
        fun getStartIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupClickListeners()
        presenter.onAttach(this)

        initializeBottomNavigationView()


        mainViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    MainVPAdapter.TREND_FRAGMENT_INDEX -> openTrend(position)
                    MainVPAdapter.EVENT_FRAGMENT_INDEX -> openEvents(position)
                    MainVPAdapter.ADD_FRAGMENT_INDEX -> openAdd(position)
                    MainVPAdapter.NOTIFICATION_FRAGMENT_INDEX -> openNotification(position)
                    MainVPAdapter.SETTINGS_FRAGMENT_INDEX -> openSettings(position)
                }
            }
        })
    }

    fun setupClickListeners() {
        mainCTB.setIconStartOnClickListener(this)
    }

    private fun initializeBottomNavigationView() {
        bottomNavView.enableAnimation(false)
        bottomNavView.isItemHorizontalTranslationEnabled = false
        bottomNavView.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        bottomNavView.setTextSize(9f)
        bottomNavView.onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
           true
        }
        mainVPAdapter = MainVPAdapter(supportFragmentManager, this)
        mainViewPager.adapter = mainVPAdapter
        mainViewPager.offscreenPageLimit = 5
        bottomNavView.setupWithViewPager(mainViewPager)
    }


    override fun onFragmentAttached(tag: String?) {
    }

    override fun onFragmentDetached(tag: String) {
        supportFragmentManager.removeFragment(tag)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector


    override fun openTrend(pageNumber: Int) {
        mainViewPager.currentItem = pageNumber
        mainCTB.setTitle(getString(R.string.trend))
    }

    override fun openEvents(pageNumber: Int) {
        mainViewPager.currentItem = pageNumber
        mainCTB.setTitle(getString(R.string.events))
    }

    override fun openAdd(pageNumber: Int) {
        mainViewPager.currentItem = pageNumber
        mainCTB.setTitle(getString(R.string.adding))
    }

    override fun openNotification(pageNumber: Int) {
        mainViewPager.currentItem = pageNumber
        mainCTB.setTitle(getString(R.string.notifications))
    }

    override fun  openSettings(pageNumber: Int) {
        mainViewPager.currentItem = pageNumber
        mainCTB.setTitle(getString(R.string.settings))
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.iconStartIV -> {
                onBackPressed()
            }
        }
    }
}