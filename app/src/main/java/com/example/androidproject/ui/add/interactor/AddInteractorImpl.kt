package com.example.androidproject.ui.add.interactor


import com.example.androidproject.data.network.ApiHelper
import com.example.androidproject.data.network.model.CategoryResponse
import com.example.androidproject.data.preferences.PreferenceHelper
import com.example.androidproject.ui.base.interactor.BaseInteractorImpl
import io.reactivex.Single
import javax.inject.Inject

class AddInteractorImpl @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractorImpl(preferenceHelper = preferenceHelper, apiHelper = apiHelper), IAddInteractor {

    override fun getCategoryResponse(): Single<CategoryResponse> = apiHelper.getCategories()
}