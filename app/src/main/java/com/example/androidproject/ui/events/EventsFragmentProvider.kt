package com.example.androidproject.ui.events

import com.example.androidproject.ui.events.view.EventsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class EventsFragmentProvider {

    @ContributesAndroidInjector(modules = [EventsFragmentModule::class])
    internal abstract fun provideEventsFragmentFactory(): EventsFragment
}