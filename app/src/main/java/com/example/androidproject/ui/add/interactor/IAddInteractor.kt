package com.example.androidproject.ui.add.interactor


import com.example.androidproject.data.network.model.CategoryResponse
import com.example.androidproject.ui.base.interactor.IInteractor
import io.reactivex.Single


interface IAddInteractor : IInteractor {
    fun getCategoryResponse(): Single<CategoryResponse>
}