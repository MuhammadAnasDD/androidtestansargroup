package com.example.androidproject.ui.events.presenter

import com.example.androidproject.ui.base.presenter.BasePresenterImpl
import com.example.androidproject.ui.events.interactor.IEventsInteractor
import com.example.androidproject.ui.events.view.IEventsView
import com.example.androidproject.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class EventsPresenterImpl<V : IEventsView, I : IEventsInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
        BasePresenterImpl<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        IEventsPresenter<V, I> {


}