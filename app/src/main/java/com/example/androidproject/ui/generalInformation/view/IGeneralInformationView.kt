package com.example.androidproject.ui.generalInformation.view

import com.example.androidproject.ui.base.view.IView

interface IGeneralInformationView : IView {

        fun setUpView()
}
