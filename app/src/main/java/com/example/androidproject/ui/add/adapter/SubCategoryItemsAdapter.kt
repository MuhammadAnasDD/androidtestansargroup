package com.example.androidproject.ui.add.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.androidproject.R
import com.example.androidproject.data.network.model.SubCategory
import com.example.androidproject.ui.generalInformation.view.GeneralInformationActivity
import kotlinx.android.synthetic.main.sun_categroy_item.view.*

class SubCategoryItemsAdapter(val mItems: MutableList<SubCategory>) : RecyclerView.Adapter<SubCategoryItemsAdapter.ItemVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVH = ItemVH(LayoutInflater.from(parent.context)
            .inflate(R.layout.sun_categroy_item, parent, false))

    override fun getItemCount(): Int = mItems.size

    override fun onBindViewHolder(holder: ItemVH, position: Int) = holder.onBind(mItems[position])

    inner class ItemVH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(sub: SubCategory) {
            sub.title?.let {
                itemView.subCategory.text = it
            }

            itemView.setOnClickListener{
                itemView.context?.let {
                    it.startActivity(GeneralInformationActivity.getStartIntent(it))
                }
            }
        }
    }

}