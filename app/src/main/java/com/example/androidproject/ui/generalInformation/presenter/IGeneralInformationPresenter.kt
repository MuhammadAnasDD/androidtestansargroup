package com.example.androidproject.ui.generalInformation.presenter

import com.example.androidproject.ui.base.presenter.IPresenter
import com.example.androidproject.ui.generalInformation.interactor.IGeneralInformationInteractor
import com.example.androidproject.ui.generalInformation.view.IGeneralInformationView

interface IGeneralInformationPresenter<V : IGeneralInformationView, I : IGeneralInformationInteractor> :
    IPresenter<V, I> {

    fun setView()
}
