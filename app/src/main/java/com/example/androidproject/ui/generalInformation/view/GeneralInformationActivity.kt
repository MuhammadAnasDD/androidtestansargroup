package com.example.androidproject.ui.generalInformation.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.androidproject.R
import com.example.androidproject.ui.base.view.BaseActivity
import com.example.androidproject.ui.generalInformation.interactor.IGeneralInformationInteractor
import com.example.androidproject.ui.generalInformation.presenter.IGeneralInformationPresenter
import com.example.androidproject.util.extension.removeFragment
import com.google.gson.Gson
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_general_information.*
import javax.inject.Inject

class GeneralInformationActivity : BaseActivity(), IGeneralInformationView,
    HasSupportFragmentInjector {

    @Inject
    lateinit var mGson: Gson

    @Inject
    internal lateinit var presenter: IGeneralInformationPresenter<IGeneralInformationView, IGeneralInformationInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    companion object {
        fun getStartIntent(context: Context): Intent {
            val intent = Intent(context, GeneralInformationActivity::class.java)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_general_information)
        presenter.onAttach(this)
        presenter.setView()
    }

    override fun onFragmentAttached(tag: String?) {
    }

    override fun onFragmentDetached(tag: String) {
        supportFragmentManager.removeFragment(tag)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector

    override fun setUpView() {


        inPerson.isSelected = true
        bookButton.isSelected = true
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.inPerson -> {
                    Toast.makeText(
                        applicationContext, getString(R.string.in_person),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                R.id.online -> {
                   // disabled
                }
            }
        }

        radioGroup2.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.bookButton -> {
                    Toast.makeText(
                        applicationContext, getString(R.string.book),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                R.id.dropButton -> {
                    Toast.makeText(
                        applicationContext, getString(R.string.drop_in),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        toolbarGeneral.setIconStartOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }
}

