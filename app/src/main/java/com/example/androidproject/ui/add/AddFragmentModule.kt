package com.example.androidproject.ui.add

import com.example.androidproject.ui.add.interactor.AddInteractorImpl
import com.example.androidproject.ui.add.interactor.IAddInteractor
import com.example.androidproject.ui.add.presenter.AddPresenterImpl
import com.example.androidproject.ui.add.presenter.IAddPresenter
import com.example.androidproject.ui.add.view.IAddView
import dagger.Module
import dagger.Provides

@Module
class AddFragmentModule {

    @Provides
    internal fun provideMyStationInteractor(addInteractorImpl: AddInteractorImpl): IAddInteractor = addInteractorImpl

    @Provides
    internal fun provideMyStationPresenter(addPresenterImpl: AddPresenterImpl<IAddView, IAddInteractor>)
            : IAddPresenter<IAddView, IAddInteractor> = addPresenterImpl
}