package com.example.androidproject.ui.generalInformation

import com.example.androidproject.ui.generalInformation.interactor.GeneralInformationInteractorImpl
import com.example.androidproject.ui.generalInformation.interactor.IGeneralInformationInteractor
import com.example.androidproject.ui.generalInformation.presenter.GeneralInformationPresenterImpl
import com.example.androidproject.ui.generalInformation.presenter.IGeneralInformationPresenter
import com.example.androidproject.ui.generalInformation.view.IGeneralInformationView
import dagger.Module
import dagger.Provides

@Module
class GeneralInformationActivityModule {

    @Provides
    internal fun provideGeneralInformationInteractor(generalInformationInteractorImpl: GeneralInformationInteractorImpl): IGeneralInformationInteractor = generalInformationInteractorImpl

    @Provides
    internal fun provideGeneralInformationPresenter(generalInformationPresenterImpl: GeneralInformationPresenterImpl<IGeneralInformationView, IGeneralInformationInteractor>)
            : IGeneralInformationPresenter<IGeneralInformationView, IGeneralInformationInteractor> = generalInformationPresenterImpl
}
