package com.example.androidproject.data.network

import com.example.androidproject.data.network.model.CategoryResponse
import io.reactivex.Single

interface ApiHelper {

    fun getCategories(): Single<CategoryResponse>

}