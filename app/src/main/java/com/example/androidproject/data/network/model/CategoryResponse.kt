package com.example.androidproject.data.network.model


data class CategoryResponse(
    val errorMessage: Any?,
    val paging: Any?,
    val result: List<ResultData>?,
    val serverTime: String?
)

data class ResultData(
    val id: String?,
    val subCategories: List<SubCategory>?,
    val title: String?,
    var isOpenSubCategory: Boolean = false
)



data class SubCategory(
    val id: String?,
    val title: String?
)