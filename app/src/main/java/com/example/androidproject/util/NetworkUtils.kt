package com.example.androidproject.util

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtils {
    /**
     * @param context: Context
     * @return returns if the user device is connected to the internet
     */
    fun isNetworkConnected(context: Context): Boolean {
        val cm: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm.activeNetworkInfo != null){
            return cm.activeNetworkInfo.isConnected
        }
        return false
    }
}
