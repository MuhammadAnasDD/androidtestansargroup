package com.example.androidproject.di.builder

import com.example.androidproject.ui.add.AddFragmentProvider
import com.example.androidproject.ui.events.EventsFragmentProvider
import com.example.androidproject.ui.generalInformation.GeneralInformationActivityModule
import com.example.androidproject.ui.generalInformation.view.GeneralInformationActivity
import com.example.androidproject.ui.main.MainActivityModule
import com.example.androidproject.ui.main.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(EventsFragmentProvider::class), (MainActivityModule::class), (AddFragmentProvider::class)])
    abstract fun bindMainActivityModule(): MainActivity

    @ContributesAndroidInjector(modules = [(GeneralInformationActivityModule::class)])
    abstract fun bindGeneralActivityModule(): GeneralInformationActivity

}